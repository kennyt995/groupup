﻿using System;

namespace GroupUpMobile.Models
{
    public class EventModel
    {

        public EventModel(string eventName, string eventDescription, string eventType, int eventSpotAvailable, DateTime eventDateTime, string eventLocation)
        {
            EventName = eventName;
            EventDescription = eventDescription;
            EventType = eventType;
            EventSpotAvailable = eventSpotAvailable;
            EventDateTime = eventDateTime;
            EventLocation = eventLocation;
        }

        public string Id { get; private set; }
        public string EventName { get; private set; }
        public string EventDescription { get; private set; }
        public string EventType { get; set; }
        public int EventSpotAvailable { get; private set; }
        public DateTime EventDateTime { get; private set; }
        public string EventLocation { get; private set; }
        public string[] EventAttendeesId { get; private set; }
        public string UserId { get; private set; }
    }
}