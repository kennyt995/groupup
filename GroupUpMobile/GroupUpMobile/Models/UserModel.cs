﻿using System;

namespace GroupUpMobile.Models
{
    public class UserModel
    {
        public UserModel(string email, string password, string confirmPassword, string firstName, string lastName, string gender, DateTime dOB)
        {
            Email = email;
            Password = password;
            ConfirmPassword = confirmPassword;
            FirstName = firstName;
            LastName = lastName;
            Gender = gender;
            DOB = dOB;
        }

        public string Email { get; private set; }
        public string Password { get; private set; }
        public string ConfirmPassword { get; private set; }
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public string Gender { get; private set; }
        public DateTime DOB { get; private set; }
    }
}
