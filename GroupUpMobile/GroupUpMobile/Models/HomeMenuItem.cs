﻿namespace GroupUpMobile.Models
{
    public enum MenuItemType
    {
        BrowsePage,
        ProfilePage,
        CreateEventPage,
    }
    public class HomeMenuItem
    {
        public MenuItemType Id { get; set; }

        public string Title { get; set; }
    }
}
