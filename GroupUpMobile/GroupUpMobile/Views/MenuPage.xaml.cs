﻿using GroupUpMobile.Models;
using GroupUpMobile.Services;
using GroupUpMobile.Views.UserViews;
using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace GroupUpMobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MenuPage : ContentPage
    {
        MainPage RootPage { get => Application.Current.MainPage as MainPage; }
        List<HomeMenuItem> menuItems;
        public MenuPage()
        {
            InitializeComponent();

            menuItems = new List<HomeMenuItem>
            {
                new HomeMenuItem {Id = MenuItemType.BrowsePage, Title="Browse Page" },
                new HomeMenuItem {Id = MenuItemType.ProfilePage, Title="Profile Page" },
                new HomeMenuItem {Id = MenuItemType.CreateEventPage, Title="Create Event Page" }
            };

            ListViewMenu.ItemsSource = menuItems;

            ListViewMenu.SelectedItem = menuItems[0];
            ListViewMenu.ItemSelected += async (sender, e) =>
            {
                if (e.SelectedItem == null)
                {
                    return;
                }

                var id = (int)((HomeMenuItem)e.SelectedItem).Id;
                await RootPage.NavigateFromMenu(id);
            };

        }

        public void Logout(object sender, EventArgs e)
        {
            Settings.AccessToken = string.Empty;
            Settings.Email = string.Empty;
            Settings.Password = string.Empty;

            App.Current.MainPage = new LoginPage();
        }
    }
}