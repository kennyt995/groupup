﻿
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace GroupUpMobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AboutPage : ContentPage
    {
        public AboutPage()
        {
            InitializeComponent();
        }
    }
}