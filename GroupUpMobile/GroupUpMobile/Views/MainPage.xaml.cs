﻿using GroupUpMobile.Models;
using GroupUpMobile.Services;
using GroupUpMobile.Views.EventViews;
using GroupUpMobile.Views.UserViews;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace GroupUpMobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : MasterDetailPage
    {
        Dictionary<int, NavigationPage> MenuPages = new Dictionary<int, NavigationPage>();
        public MainPage()
        {
            InitializeComponent();

            MasterBehavior = MasterBehavior.Popover;

            MenuPages.Add((int)MenuItemType.BrowsePage, (NavigationPage)Detail);
        }

        public async Task NavigateFromMenu(int id)
        {
            if (!MenuPages.ContainsKey(id))
            {
                switch (id)
                {
                    case (int)MenuItemType.BrowsePage:
                        MenuPages.Add(id, new NavigationPage(new EventsPage()));
                        break;
                    case (int)MenuItemType.ProfilePage:
                        MenuPages.Add(id, new NavigationPage(new ProfilePage()));
                        break;
                    case (int)MenuItemType.CreateEventPage:
                        MenuPages.Add(id, new NavigationPage(new CreateEventPage()));
                        break;
                }
            }

            var newPage = MenuPages[id];

            if (newPage != null && Detail != newPage)
            {
                Detail = newPage;

                if (Device.RuntimePlatform == Device.Android)
                {
                    await Task.Delay(100);
                }

                IsPresented = false;
            }
        }

        private void Logout()
        {
            Settings.AccessToken = string.Empty;
            Settings.Email = string.Empty;
            Settings.Password = string.Empty;
            new NavigationPage(new LoginPage());
        }
    }
}