﻿using GroupUpMobile.Services;
using GroupUpMobile.shared.api;
using System;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace GroupUpMobile.Views.UserViews
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {

        private UserApi userApi = new UserApi();
        private string loginErrorMessage;

        public string Email { get; set; }
        public string Password { get; set; }
        public string LoginErrorMessage
        {
            get => loginErrorMessage;
            set
            {
                if (loginErrorMessage != value)
                {
                    loginErrorMessage = value;
                    OnPropertyChanged();
                }
            }
        }

        public LoginPage()
        {
            InitializeComponent();
            Title = "Login";
            Email = Settings.Email;
            Password = Settings.Password;

            BindingContext = this;
        }

        public ICommand LoginCommand => new Command(async () =>
        {
            if (!string.IsNullOrEmpty(Settings.AccessToken = await userApi.LoginAsync(Email, Password)))
            {
                App.Current.MainPage = new MainPage();
            }
            else { LoginErrorMessage = "The user name or password is incorrect"; }

        });

        private void RegisterButton(object sender, EventArgs e) => App.Current.MainPage = new RegisterPage();
    }
}