﻿using GroupUpMobile.Models;
using GroupUpMobile.Services;
using GroupUpMobile.shared.api;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace GroupUpMobile.Views.UserViews
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RegisterPage : ContentPage
    {

        private UserApi userApi = new UserApi();

        public string Email { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public DateTime DOB { get; set; }

        private string emailErrorMessage;
        private string passwordErrorMessage;
        private string passwordMatchedErrorMessage;
        private string firstNameErrorMessage;
        private string lastnameErrorMessage;
        private string genderErrorMessage;
        private string dobErrorMessage;

        public string EmailErrorMessage
        {
            get => emailErrorMessage;
            private set
            {
                if (emailErrorMessage != value)
                {
                    emailErrorMessage = value;
                    OnPropertyChanged();
                }
            }
        }

        public string PasswordErrorMessage
        {
            get => passwordErrorMessage;
            private set
            {
                if (passwordErrorMessage != value)
                {
                    passwordErrorMessage = value;
                    OnPropertyChanged();
                }
            }
        }
        public string PasswordMatchedErrorMessage
        {
            get => passwordMatchedErrorMessage;
            private set
            {
                if (passwordMatchedErrorMessage != value)
                {
                    passwordMatchedErrorMessage = value;
                    OnPropertyChanged();
                }
            }
        }
        public string FirstNameErrorMessage
        {
            get => firstNameErrorMessage;
            private set
            {
                if (firstNameErrorMessage != value)
                {
                    firstNameErrorMessage = value;
                    OnPropertyChanged();
                }
            }
        }
        public string LastnameErrorMessage
        {
            get => lastnameErrorMessage;
            private set
            {
                if (lastnameErrorMessage != value)
                {
                    lastnameErrorMessage = value;
                    OnPropertyChanged();
                }
            }
        }
        public string GenderErrorMessage
        {
            get => genderErrorMessage;
            private set
            {
                if (genderErrorMessage != value)
                {
                    genderErrorMessage = value;
                    OnPropertyChanged();
                }
            }
        }
        public string DOBErrorMessage
        {
            get => dobErrorMessage;
            private set
            {
                if (dobErrorMessage != value)
                {
                    dobErrorMessage = value;
                    OnPropertyChanged();
                }
            }
        }
        public RegisterPage()
        {
            InitializeComponent();
            Title = "Register Page";

            GenderPicker.Items.Add("Male");
            GenderPicker.Items.Add("Female");
            GenderPicker.Items.Add("Other");

            BindingContext = this;
        }


        private void LoginButon(object sender, EventArgs e) => App.Current.MainPage = new LoginPage();

        public ICommand RegisterCommand => new Command(async () =>
        {

            UserModel user = new UserModel(Email, Password, ConfirmPassword, FirstName, LastName, Gender, DOB);

            HttpResponseMessage isSuccess = await userApi.RegisterAsync(user);
            if (!isSuccess.IsSuccessStatusCode)
            {

                string content = isSuccess.Content.ReadAsStringAsync().Result;
                dynamic objJson = JsonConvert.DeserializeObject(content);

                EmailErrorMessage = (string)string.Join("", objJson.modelState["model.Email"] ?? objJson.modelState[""] ?? "");
                PasswordErrorMessage = (string)string.Join("", objJson.modelState["model.Password"] ?? "");
                PasswordMatchedErrorMessage = (string)string.Join("", objJson.modelState["model.ConfirmPassword"] ?? "");
                FirstNameErrorMessage = (string)string.Join("", objJson.modelState["model.FirstName"] ?? "");
                LastnameErrorMessage = (string)string.Join("", objJson.modelState["model.LastName"] ?? "");
                GenderErrorMessage = (string)string.Join("", objJson.modelState["model.Gender"] ?? "");
                DOBErrorMessage = (string)string.Join("", objJson.modelState["model.DOB"] ?? "");
            }
            else
            {
                Settings.Email = Email;
                Settings.Password = Password;
                App.Current.MainPage = new LoginPage();
                //await Navigation.PushAsync(new LoginPage());
            }

        });

    }
}