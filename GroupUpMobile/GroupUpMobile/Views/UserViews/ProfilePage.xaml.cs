﻿
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace GroupUpMobile.Views.UserViews
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProfilePage : ContentPage
    {
        public ProfilePage()
        {
            InitializeComponent();

            Title = "Profile Page";

            BindingContext = this;
        }
    }
}