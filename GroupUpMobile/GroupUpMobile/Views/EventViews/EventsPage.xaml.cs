﻿using GroupUpMobile.Models;
using System;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace GroupUpMobile.Views.EventViews
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EventsPage : ContentPage
    {
        public ObservableCollection<EventModel> Events { get; set; }
        public Command LoadEventsCommand { get; set; }


        public EventsPage()
        {
            InitializeComponent();
            Title = "Browse";
            BindingContext = this;
        }

        async void OnEventSelected(object sender, SelectedItemChangedEventArgs args)
        {
            if (!(args.SelectedItem is EventModel @event))
            {
                return;
            }

            await Navigation.PushAsync(new EventDetailPage(@event));

            // Manually deselect item.
            EventsListView.SelectedItem = null;
        }

        async void AddEvent_Clicked(object sender, EventArgs e) => await Navigation.PushModalAsync(new NavigationPage(new CreateEventPage()));


    }
}