﻿using GroupUpMobile.Models;
using GroupUpMobile.shared.api;
using System;
using System.Diagnostics;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace GroupUpMobile.Views.EventViews
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CreateEventPage : ContentPage
    {
        public EventModel Event { get; set; }
        private readonly EventApi eventApi = new EventApi();

        private string eventNameErrorMessage;
        private string eventDescriptionErrorMessage;
        private string eventTypeErrorMessage;
        private string locationErrorMessage;
        private string spotAvailableErrorMessage;

        private string eventDateTimeErrorMessage;

        private bool _isLimited;
        private bool _isNotRepeat;
        private bool _isRepeat;

        public string EventName { get; set; }
        public string EventDescription { get; set; }
        public string EventType { get; set; }
        public string Location { get; set; }
        public string IsLimited
        {
            get => _IsLimited ? "Yes" : "No";
            set => _IsLimited = value == "Yes" ? true : false;
        }
        public bool _IsLimited
        {
            get => _isLimited;
            set
            {
                if (_isLimited != value)
                {
                    _isLimited = value;
                    OnPropertyChanged();
                }
            }
        }
        public int SpotAvailable { get; set; }
        public string IsRepeat
        {
            get => _IsRepeat ? "Yes" : "No";
            set
            {
                if (value == "Yes")
                {
                    _IsRepeat = true;
                    _IsNotRepeat = false;
                }
                else
                {
                    _IsRepeat = false;
                    _IsNotRepeat = true;
                }
            }
        }
        public bool _IsRepeat
        {
            get => _isRepeat;
            set
            {
                if (_isRepeat != value)
                {
                    _isRepeat = value;
                    OnPropertyChanged();
                }
            }
        }
        public bool _IsNotRepeat
        {
            get => _isNotRepeat;
            set
            {
                if (_isNotRepeat != value)
                {
                    _isNotRepeat = value;
                    OnPropertyChanged();
                }
            }
        }

        public int RepeatingWeeks { get; set; }
        public DateTime EventDateTime { get; set; }
        public TimeSpan MondaysStartTime { get; set; }
        public TimeSpan MondaysEndTime { get; set; }
        public TimeSpan TuesdaysStartTime { get; set; }
        public TimeSpan TuesdaysEndTime { get; set; }
        public TimeSpan WednesdaysStartTime { get; set; }
        public TimeSpan WednesdaysEndTime { get; set; }
        public TimeSpan ThursdaysStartTime { get; set; }
        public TimeSpan ThursdaysEndTime { get; set; }
        public TimeSpan FridaysStartTime { get; set; }
        public TimeSpan FridaysEndTime { get; set; }
        public TimeSpan SaturdaysStartTime { get; set; }
        public TimeSpan SaturdaysEndTime { get; set; }
        public TimeSpan SundaysStartTime { get; set; }
        public TimeSpan SundaysEndTime { get; set; }

        public string EventNameErrorMessage
        {
            get => eventNameErrorMessage;
            set
            {
                if (eventNameErrorMessage != value)
                {
                    eventNameErrorMessage = value;
                    OnPropertyChanged();
                }
            }
        }

        public string EventDescriptionErrorMessage
        {
            get => eventDescriptionErrorMessage;
            set
            {
                if (eventDescriptionErrorMessage != value)
                {
                    eventDescriptionErrorMessage = value;
                    OnPropertyChanged();
                }
            }
        }

        public string EventTypeErrorMessage
        {
            get => eventTypeErrorMessage;
            set
            {
                if (eventTypeErrorMessage != value)
                {
                    eventTypeErrorMessage = value;
                    OnPropertyChanged();
                }
            }
        }

        public string LocationErrorMessage
        {
            get => locationErrorMessage;
            set
            {
                if (locationErrorMessage != value)
                {
                    locationErrorMessage = value;
                    OnPropertyChanged();
                }
            }
        }

        public string SpotAvailableErrorMessage
        {
            get => spotAvailableErrorMessage;
            set
            {
                if (spotAvailableErrorMessage != value)
                {
                    spotAvailableErrorMessage = value;
                    OnPropertyChanged();
                }
            }
        }

        public string EventDateTimeErrorMessage
        {
            get => eventDateTimeErrorMessage;
            set
            {
                if (eventDateTimeErrorMessage != value)
                {
                    eventDateTimeErrorMessage = value;
                    OnPropertyChanged();
                }
            }
        }

        public CreateEventPage()
        {
            InitializeComponent();

            EventTypePicker.Items.Add("Event");
            EventTypePicker.Items.Add("Project");

            IsLimitedPicker.Items.Add("Yes");
            IsLimitedPicker.Items.Add("No");

            IsRepeatPicker.Items.Add("Yes");
            IsRepeatPicker.Items.Add("No");

            BindingContext = this;
        }

        public ICommand CreateEvent => new Command(() =>
       {
           Debug.WriteLine(
               "Event name: " + EventName +
                "\nEvent description: " + EventDescription +
                "\nEvent type: " + EventType +
                "\nEvent location: " + Location +
                "\nEvent repeats: " + IsRepeat +
                "\nEvent spot: " + SpotAvailable +
                "\nEvent repeat: " + IsRepeat +
                "\nEvent datetime: " + EventDateTime +
                "\nEvent weeks: " + RepeatingWeeks +
                "\nEvent monday: " + MondaysStartTime
               );

           //EventModel @event = new EventModel(EventName, EventDescription, EventType, SpotAvailable, EventDateTime, Location);

           //HttpResponseMessage isSuccess = await eventApi.PostEventAsync(@event, Settings.AccessToken);
           //if (!isSuccess.IsSuccessStatusCode)
           //{
           //    string content = isSuccess.Content.ReadAsStringAsync().Result;
           //    dynamic objJson = JsonConvert.DeserializeObject(content);

           //    EventNameErrorMessage = (string)string.Join("", objJson.modelState["model.EventName"] ?? "");
           //    EventDescriptionErrorMessage = (string)string.Join("", objJson.modelState["model.EventDescription"] ?? "");
           //    EventDateTimeErrorMessage = (string)string.Join("", objJson.modelState["model.EventDateTime"] ?? "");
           //    EventLocationErrorMessage = (string)string.Join("", objJson.modelState["model.EventLocation"] ?? "");
           //}

       });


    }

}
