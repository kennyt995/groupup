﻿using GroupUpMobile.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace GroupUpMobile.Views.EventViews
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EventDetailPage : ContentPage
    {
        private EventModel Event;
        public EventDetailPage(EventModel @event)
        {
            InitializeComponent();
            Title = @event?.EventName;
            Event = @event;

            BindingContext = this;
        }

    }
}