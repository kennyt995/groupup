﻿using Plugin.Settings;
using Plugin.Settings.Abstractions;


namespace GroupUpMobile.Services
{
    public static class Settings
    {
        private static ISettings AppSettings => CrossSettings.IsSupported ? CrossSettings.Current : null;

        public static string Email
        {
            get => AppSettings.GetValueOrDefault("Email", "");

            set => AppSettings.AddOrUpdateValue("Email", value);
        }

        public static string Password
        {
            get => AppSettings.GetValueOrDefault("Password", "");

            set => AppSettings.AddOrUpdateValue("Password", value);
        }
        public static string AccessToken
        {
            get => AppSettings.GetValueOrDefault("AccessToken", "");

            set => AppSettings.AddOrUpdateValue("AccessToken", value);
        }
    }
}