﻿
using GroupUpMobile.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Diagnostics;
using System.Dynamic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace GroupUpMobile.shared.api
{
    class UserApi : DynamicObject
    {
        public async Task<HttpResponseMessage> RegisterAsync(UserModel user)
        {
            Debug.WriteLine("UserApi line 16 " + user);
            HttpClient client = new HttpClient();
            Debug.WriteLine("UserApi line 16 " + user);

            string json = JsonConvert.SerializeObject(user);
            Debug.WriteLine("UserApi line 22 " + json);

            HttpContent content = new StringContent(json);
            Debug.WriteLine("UserApi line 25 " + content);

            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            HttpResponseMessage response = await client.PostAsync(Constants.baseURL + "/api/Register", content);
            Debug.WriteLine("UserApi line 29 " + response);

            return response;
        }

        public async Task<string> LoginAsync(string email, string password)
        {
            List<KeyValuePair<string, string>> keyValues = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("username", email),
                new KeyValuePair<string, string>("password", password)
            };

            //HTTP TO CHANGE
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, Constants.baseURL + "/api/login")
            {
                Content = new FormUrlEncodedContent(keyValues)
            };

            HttpClient client = new HttpClient();
            HttpResponseMessage response = await client.SendAsync(request);
            string jwt = await response.Content.ReadAsStringAsync();
            dynamic jwtDynamic = JsonConvert.DeserializeObject<dynamic>(jwt);
            string accessToken = jwtDynamic.Value<string>("access_token");
            return accessToken;
        }

        //public async Task<string> GetUserInfo(string accessToken)
        //{

        //}

    }
}
