﻿using GroupUpMobile.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace GroupUpMobile.shared.api
{
    class EventApi
    {
        public async Task<List<EventModel>> GetUserCreatedEventsAsync(string accessToken)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            string json = await client.GetStringAsync(Constants.baseURL + "/api/Events/UserCreatedEvents");

            List<EventModel> events = JsonConvert.DeserializeObject<List<EventModel>>(json);

            return events;
        }

        public async Task<HttpResponseMessage> PostEventAsync(EventModel @event, string accessToken)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            string json = JsonConvert.SerializeObject(@event);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new MediaTypeHeaderValue("applicatoin/json");

            HttpResponseMessage response = await client.PostAsync(Constants.baseURL + "/api/Event", content);

            return response;
        }
    }
}

