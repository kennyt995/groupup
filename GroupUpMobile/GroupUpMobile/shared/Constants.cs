﻿namespace GroupUpMobile.shared.api
{
    class Constants
    {
        //this works for android, ios and window phone



        //------Login--------------
        public static string LoginUrl = "https://text.com/api/Auth/Login";

        public static string NoInternetText = "No Internet, please reconnect.";

        public static string SettingsScreenTitle = "Settings";

        public static string baseURL = "https://localhost:44366";

        //public static string baseURL = "https://localhost:5001";
    }
}
