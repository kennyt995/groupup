﻿using Dna;
using GroupUp.Data;
using GroupUp.Email;
using Microsoft.Extensions.DependencyInjection;

namespace GroupUp.IoC
{
    /// <summary>
    /// A shorthand access class to get DI services with nice clean short code
    /// </summary>
    public static class DI
    {
        public static ApplicationDbContext ApplicationDbContext => Framework.Provider.GetService<ApplicationDbContext>();

        public static IEmailSender EmailSender => Framework.Provider.GetService<IEmailSender>();

        public static IEmailTemplateSender EmailTemplateSender => Framework.Provider.GetService<IEmailTemplateSender>();
    }
}
