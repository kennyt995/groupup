﻿using System.Threading.Tasks;

namespace GroupUp.Email
{
    public interface IEmailTemplateSender
    {
        Task<SentEmailResponse> SendGeneralEmailAsync(SendEmailDetails details, string title, string content1, string content2, string buttonText, string buttonUrl);
    }
}
