﻿using Microsoft.Extensions.DependencyInjection;

namespace GroupUp.Email.Templates
{
    public static class EmailTemplateSenderExtensions
    {
        public static IServiceCollection AddEmailTemplateSender(this IServiceCollection services)
        {
            services.AddTransient<IEmailTemplateSender, EmailTemplateSender>();

            return services;
        }
    }
}
