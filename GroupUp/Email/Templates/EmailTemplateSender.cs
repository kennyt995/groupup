﻿using GroupUp.IoC;
using System;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace GroupUp.Email.Templates
{
    public class EmailTemplateSender : IEmailTemplateSender
    {
        public async Task<SentEmailResponse> SendGeneralEmailAsync(SendEmailDetails details, string title, string content1, string content2, string buttonText, string buttonUrl)
        {
            var templateText = default(string);

            Stream htmFile = Assembly.GetEntryAssembly().GetManifestResourceStream("GroupUp.Email.Templates.GeneralTemplate.htm");

            if (htmFile == null)
            {
                throw new Exception("Can't find template");
            }

            using (var reader = new StreamReader(htmFile, Encoding.UTF8))
            {
                templateText = await reader.ReadToEndAsync();
            }

            templateText = templateText.Replace("--Title--", title)
                                        .Replace("--Content1--", content1)
                                        .Replace("--Content2--", content2)
                                        .Replace("--ButtonText--", buttonText)
                                        .Replace("--ButtonUrl--", buttonUrl);
            details.Content = templateText;

            return await DI.EmailSender.SendEmailAsync(details);
        }
    }
}
