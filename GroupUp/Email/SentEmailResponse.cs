﻿using System.Collections.Generic;

namespace GroupUp.Email
{
    public class SentEmailResponse
    {
        public bool Successful => !(Errors?.Count > 0);

        public List<string> Errors { get; set; }
    }
}
