﻿using Newtonsoft.Json;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using static Dna.FrameworkDI;

namespace GroupUp.Email
{
    public class SendGridEmailSender : IEmailSender
    {
        public async Task<SentEmailResponse> SendEmailAsync(SendEmailDetails details)
        {
            var apiKey = Configuration["SendGridKey"];
            var client = new SendGridClient(apiKey);
            var from = new EmailAddress(details.FromEmail, details.FromName);
            var to = new EmailAddress(details.ToEmail, details.ToName);
            var subject = details.Subject;
            var content = details.Content;
            var msg = MailHelper.CreateSingleEmail(
                from,
                to,
                subject,
                details.IsHTML ? null : details.Content,
                details.IsHTML ? details.Content : null);
            var response = await client.SendEmailAsync(msg);
            if (response.StatusCode == System.Net.HttpStatusCode.Accepted)
                return new SentEmailResponse();
            try
            {
                var bodyResult = await response.Body.ReadAsStringAsync();
                var sendGridResponse = JsonConvert.DeserializeObject<SendGridResponse>(bodyResult);
                var errorResponse = new SentEmailResponse
                {
                    Errors = sendGridResponse?.Errors.Select(f => f.Message).ToList()
                };
                if (errorResponse.Errors == null || errorResponse.Errors.Count == 0)
                    errorResponse.Errors = new List<string>(new[] { "Unknown error from email sending service. Please contact Fasetto support." });
                return errorResponse;
            }
            catch (Exception ex)
            {
                if (Debugger.IsAttached)
                {
                    var error = ex;
                    Debugger.Break();
                }
                return new SentEmailResponse
                {
                    Errors = new List<string>(new[] { "Unknown error occurred" })
                };
            }
        }
    }
}
