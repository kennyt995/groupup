﻿using System.Threading.Tasks;

namespace GroupUp.Email
{
    public interface IEmailSender
    {
        Task<SentEmailResponse> SendEmailAsync(SendEmailDetails details);
    }
}
