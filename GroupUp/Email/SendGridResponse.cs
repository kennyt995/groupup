﻿using System.Collections.Generic;

namespace GroupUp.Email
{
    public class SendGridResponse
    {
        public List<SendGridResponseError> Errors { get; set; }
    }
}
