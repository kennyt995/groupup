﻿using GroupUp.Email;
using GroupUp.IoC;
using System.Threading.Tasks;
using static Dna.FrameworkDI;

namespace GroupUp.Controllers
{
    public class GroupUpEmailSender
    {
        public static async Task<SentEmailResponse> SendUserVerificationEmailAsync(string displayName, string email, string verificationUrl) => await DI.EmailTemplateSender.SendGeneralEmailAsync(new SendEmailDetails
        {
            IsHTML = true,
            FromEmail = Configuration["GroupUpSettings:SendEmailFromEmail"],
            FromName = Configuration["GroupUpSettings:SendEmailFromName"],
            ToEmail = "KennyT995@gmail.com",
            ToName = displayName,
            Subject = "Verify Your Email - GroupUp"
        },
            "Verify Email",
            $"Hi {displayName ?? "stranger"},",
            "Thanks for creating an account with us.<br/>To continue please verify your email with us.",
            "Verify Email",
            verificationUrl
            );
    }
}
