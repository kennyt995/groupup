﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GroupUp.Identity
{
    public static class IdentityErrorExtensions
    {
        public static string AggregateErrors(this IEnumerable<IdentityError> errors)
        {
            return errors?.ToList()
                          .Select(f => f.Description)
                          .Aggregate((a, b) => $"{a}{Environment.NewLine}{b}");
        }
    }
}
