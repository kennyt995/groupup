﻿using System;
using System.ComponentModel.DataAnnotations;

namespace GroupUp.Models
{
    public class EventModel
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(256)]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please enter a description of event")]
        [MaxLength(2048)]
        public string Description { get; set; }

        [Required(ErrorMessage = "Please pick a event type")]
        [MaxLength(256)]
        public string Type { get; set; }

        [Required]
        public bool Repeat { get; set; }

        public string SpotAvailable { get; set; }

        [Required(ErrorMessage = "Please enter a date")]
        [DataType(DataType.Date)]
        [Display(Name = "EventDateTime")]
        [DisplayFormat(DataFormatString = "{hh.mm.ss}")]
        [MaxLength(256)]
        public DateTime DateTime { get; set; }

        [Required(ErrorMessage = "Please enter a location")]
        [MaxLength(256)]
        public string Location { get; set; }

        [Required]
        public string AttendeesId { get; set; }

        [Required]
        public string CreatorId { get; set; }


    }
}