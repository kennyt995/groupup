﻿namespace GroupUp.Controllers
{
    public class ApiResponse
    {
        #region Public Properties

        public bool Successful => ErrorMessages == null;

        public string ErrorMessages { get; set; }

        public object Response { get; set; }

        public ApiResponse()
        {

        }

        #endregion
    }
    public class ApiResponse<T> : ApiResponse
    {
        public new T Response
        {
            get => (T)base.Response;
            set => base.Response = value;
        }
    }
}
