﻿using GroupUp.Authentication;
using GroupUp.Data;
using GroupUp.Identity;
using GroupUp.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace GroupUp.Controllers
{

    public class AuthController : Controller
    {
        protected ApplicationDbContext context;
        protected UserManager<UserModel> userManager;
        protected SignInManager<UserModel> signInManager;

        public AuthController(
        ApplicationDbContext context,
        UserManager<UserModel> userManager,
        SignInManager<UserModel> signInManager)
        {
            this.context = context;
            this.userManager = userManager;
            this.signInManager = signInManager;
        }


        [AllowAnonymous]
        [Route("api/Register")]
        [HttpPost]
        public async Task<ApiResponse<LoginModel>> RegisterAsync([FromBody]UserModel user)
        {
            if (ModelState.IsValid)
            {
                user.UserName = user.Email;
                var result = await userManager.CreateAsync(user, user.Password);

                if (result.Succeeded)
                {
                    UserModel userIdentity = await userManager.FindByNameAsync(user.UserName);
                    // await SendVerificatoinEmailAsync(user);

                    return new ApiResponse<LoginModel>
                    {
                        Response = new LoginModel
                        {
                            FirstName = userIdentity.FirstName,
                            LastName = userIdentity.LastName,
                            Email = userIdentity.Email,
                            Username = userIdentity.UserName,
                            Token = userIdentity.GenerateJwtToken()
                        }
                    };
                }
                else
                {
                    return new ApiResponse<LoginModel>
                    {
                        ErrorMessages = result.Errors.AggregateErrors()
                    };
                }
            }
            else
            {
                var errorList = ModelState.ToDictionary(
                    kvp => kvp.Key,
                    kvp => kvp.Value.Errors.Select(e => e.ErrorMessage).ToArray()
                    );
                return new ApiResponse<LoginModel>
                {
                    ErrorMessages = errorList.ToString()
                };
            }

        }

        [AllowAnonymous]
        [Route("api/login")]
        [HttpGet]
        public async Task<string> LoginAsync(string email, string password)
        {
            var user = await userManager.FindByEmailAsync(email);
            var isValidPassword = await userManager.CheckPasswordAsync(user, password);
            return user.GenerateJwtToken();
        }

        private async Task SendVerificatoinEmailAsync(UserModel user)
        {
            var userIdentity = await userManager.FindByNameAsync(user.UserName);

            string emailVerificationCode = await userManager.GenerateEmailConfirmationTokenAsync(user);

            string verificationUrl = $"https://{Request.Host.Value}/api/verify/email/?userId={HttpUtility.UrlEncode(userIdentity.Id)}&emailToken={HttpUtility.UrlEncode(emailVerificationCode)}";


            await GroupUpEmailSender.SendUserVerificationEmailAsync(user.UserName, userIdentity.Email, verificationUrl);
        }

        [AllowAnonymous]
        [Route("api/verify/email")]
        [HttpGet]
        public async Task<ActionResult> VerifyEmailAsync(string userId, string emailToken)
        {
            var user = await userManager.FindByIdAsync(userId);
            if (user == null)
            {
                return Content("User not found");

            }

            var result = await userManager.ConfirmEmailAsync(user, emailToken);
            if (result.Succeeded)
            {
                return Content("Email Verified :)");

            }
            return Content("Invalid Email Verification Token :(");
        }

    }
}
