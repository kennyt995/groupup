import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute, Router } from '@angular/router';

import UserService from '../shared/api/userService';
import User from '../shared/models/UserModel';
import { HttpErrorResponse } from '@angular/common/http';

declare var require: any;

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.sass']
})
export class RegisterComponent implements OnInit {

  user: User = new User();

  sub: Subscription;
  emailErrorMessage = '';
  passwordErrorMessage = '';
  passwordMatchedErrorMessage = '';

  constructor(private route: ActivatedRoute,
    private router: Router,
    private userService: UserService) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {

    });
  }

  gotoLogin() {
    this.router.navigate(['/login']);
  }

  register() {

    this.userService.register(this.user).subscribe(
      result => {
        console.log(result);
        this.gotoLogin();
      },
      (response: any) => {
        console.log('register component ts line 47 ' + JSON.stringify(response));
        this.emailErrorMessage = response.error.errors.Email || '';
        this.passwordErrorMessage = response.error.errors.Password || '';
        this.passwordMatchedErrorMessage = response.error.errors.ConfirmPassword || '';
      });
  }

}
