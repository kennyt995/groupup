import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute, Router } from '@angular/router';

import UserService from '../shared/api/userService';
import { HttpErrorResponse } from '@angular/common/http';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {

  sub: Subscription;
  isLoginError: boolean = false;
  isEmailError: boolean = false;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private userService: UserService) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {

    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  gotoRegister() {
    this.router.navigate(['/register']);
  }

  login(form: any) {


    this.userService.login(form.username, form.password).subscribe(
      (result: any) => {
        console.log('login.ts line 41' + result);
        localStorage.setItem('userToken', result.access_token);
        this.gotoRegister();
      },
      (error: HttpErrorResponse) => {
        this.isLoginError = true;
      });
  }
}
