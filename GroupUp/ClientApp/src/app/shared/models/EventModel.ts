export default class Event {
    id: number;
    eventName: string;
    dateTime: string;
    location: string;
    description: string;
    eventType: string;
    userId: string;
  }