import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import User from '../models/UserModel';
import Constants from '../constants';


@Injectable()
export default class UserService {
  constructor(private http: HttpClient) { }

  getAll(): Observable<Array<User>> {
    return this.http.get<Array<User>>(Constants.baseURL);
  }

  register(user: User) {
    return this.http.post<User>(Constants.baseURL + '/api/Register', user);
  }

  login(username: string, password: string) {
    const data = `/?email=${username}&password=${password}`;
    console.log(data);
    return this.http.get(`${Constants.baseURL}/api/login` + data);
  }

  save(user: User): Observable<User> {
    let result: Observable<User>;
    if (user.id) {
      result = this.http.put<User>(
        `${Constants.baseURL}/${user.id}`,
        event
      );
    } else {
      result = this.http.post<User>(Constants.baseURL, user);
    }
    return result;
  }

  remove(id: number) {
    return this.http.delete(`${Constants.baseURL}/${id.toString()}`);
  }
}
