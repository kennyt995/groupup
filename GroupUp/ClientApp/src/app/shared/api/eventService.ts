import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import Event from '../models/EventModel';
import Constants from '../constants';
@Injectable()
export default class EventService {
  public EVENTS_API = `${Constants.baseURL}/events`;

  constructor(private http: HttpClient) { }

  getAll(): Observable<Array<Event>> {
    return this.http.get<Array<Event>>(this.EVENTS_API);
  }

  get(id: string) {
    return this.http.get(`${this.EVENTS_API}/${id}`);
  }

  save(event: Event): Observable<Event> {
    let result: Observable<Event>;
    if (event.id) {
      result = this.http.put<Event>(
        `${this.EVENTS_API}/${event.id}`,
        event
      );
    } else {
      result = this.http.post<Event>(this.EVENTS_API, event);
    }
    return result;
  }

  remove(id: number) {
    return this.http.delete(`${this.EVENTS_API}/${id.toString()}`);
  }
}