import { Component } from '@angular/core';
import UserService from './shared/api/userService';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'GroupUp';

  isAuthenticated: boolean;

  constructor(private router: Router) { }
  // tslint:disable-next-line:use-life-cycle-interface
  ngOnInit() {
  }

  logout() {
    localStorage.removeItem('userToken');
    this.router.navigate(['/login']);
  }
}
