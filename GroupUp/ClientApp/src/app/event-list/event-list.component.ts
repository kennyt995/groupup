import { Component, OnInit } from '@angular/core';
import EventService from '../shared/api/eventService';
import Event from '../shared/models/EventModel';

@Component({
  selector: 'app-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.sass']
})
export class EventListComponent implements OnInit {

  event: Array<Event>;

  constructor(private eventService: EventService) { }

  ngOnInit() {
    this.eventService.getAll().subscribe(data => {
      this.event = data;
    });
  }

}
